import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  display = false;
  isAuth = false;

  constructor(private loginService: AuthService, private router: Router) { }

  ngOnInit() {
    this.loginService.isAuthObservable
      .subscribe((isAuth) => this.isAuth = isAuth);
  }

  showDialog() {
    this.display = true;
  }

  onLogout() {
    this.loginService.logout();
    this.router.navigate(['/']);
  }

}
