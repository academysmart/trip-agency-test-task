export interface Trip {
  title:	string;
  id:	string;
  imageUrl:	string;
  fromDate:	string;
  readonly toDate: string;
  location: string;
  price: number;
  avgRating: number;
  description:	string;
  like: boolean;
}
