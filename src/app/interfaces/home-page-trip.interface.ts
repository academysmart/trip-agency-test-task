export interface HomePageTrip {
  fromDate: string;
  id: string;
  imageUrl: string;
  location: string;
  price: number;
  title: string;
  toDate: string;
}
