import {Component, EventEmitter, Output} from '@angular/core';
import { LoginRequest } from '../interfaces/login-request.interface';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { LoginResponse } from '../interfaces/login-response.interface';
import * as moment from 'moment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
  loginRequest: LoginRequest;
  submitted = false;
  @Output() loggedIn = new EventEmitter<boolean>();
  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', Validators.required),
  });

  constructor(private loginService: AuthService, private router: Router) { }

  onSubmit() {
    this.submitted = true;

    this.loginRequest = {
      userName: this.loginForm.controls.username.value,
      password: this.loginForm.controls.password.value
    };
    if (this.loginForm.invalid) {
      return;
    }
    this.loginService.login(this.loginRequest)
      .subscribe((data: LoginResponse) => {
        this.loggedIn.emit(true);
        this.router.navigate(['/trips']);
      },
      (error) => {
        this.loginForm.setErrors({wrongCredentials: 'Wrong username or password' });
      }
    );

  }

}
