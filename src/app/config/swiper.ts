export const swiperConfig = {
  a11y: true,
  direction: 'horizontal',
  slidesPerView: 1,
  keyboard: true,
  mousewheel: false,
  scrollbar: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true
  }
}
