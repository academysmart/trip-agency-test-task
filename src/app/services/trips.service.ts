import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Trip} from '../interfaces/trip.interface';
import {Observable} from 'rxjs';
import {HomePageTrip} from '../interfaces/home-page-trip.interface';

@Injectable({
  providedIn: 'root'
})
export class TripsService {
  apiUrl: string = environment.apiUrl;
  homeTripsApiUrl: string = 'api/TripAgency/Trips';
  tripsApiUrl: string = 'api/TripAgency/Trips/Homepage';
  defaultNumberOfTrips: string = '4';
  constructor(private http: HttpClient) { }

  getTrips(): Observable<Trip[]> {
    return this.http.get<Trip[]>(`${this.apiUrl}${this.homeTripsApiUrl}`);
  }

  getHomeTrips(numberOfTrips = this.defaultNumberOfTrips): Observable<HomePageTrip[]> {
    const params = new HttpParams().set('numberOfTrips', numberOfTrips);
    return this.http.get<HomePageTrip[]>(`${this.apiUrl}${this.tripsApiUrl}`, {params});
  }
}
