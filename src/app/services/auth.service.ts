import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import * as moment from 'moment';
import {BehaviorSubject} from 'rxjs';
import {tap} from 'rxjs/operators';
import {LoginResponse} from '../interfaces/login-response.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private authSubject = new BehaviorSubject<boolean>(false);

  apiUrl: string = environment.apiUrl;
  loginApiUrl: string = '/api/TripAgency/Login';
  isAuthObservable = this.authSubject.asObservable();

  constructor(private http: HttpClient) { }

  isAuth() {
    const isAuth = !!((+localStorage.getItem('expires') > moment().unix()) && (localStorage.getItem('token')));

    this.authSubject.next(isAuth);

    return isAuth;
  }

  login(body) {
    return this.http.post(`${this.apiUrl}${this.loginApiUrl}`, body)
      .pipe(
        tap((data: LoginResponse) => {
          localStorage.setItem('token', data.accessToken);
          localStorage.setItem('expires', (moment().unix() + data.expires).toString());
        }),
      );
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expires');
  }
}
