import { Component, OnInit } from '@angular/core';
import {TripsService} from '../services/trips.service';
import {Trip} from '../interfaces/trip.interface';

@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss']
})
export class TripsComponent implements OnInit {
  trips: Trip[];

  constructor(private tripService: TripsService) { }

  ngOnInit() {
    this.tripService.getTrips()
      .subscribe((data): Trip[] => {
        return this.trips = data;
      });
  }
}
