import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './services/auth.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private loginService: AuthService, private router: Router) {}

  canActivate(route, state) {
    if (!this.loginService.isAuth()) {
      this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }
}
