import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import {HomePageTrip} from '../interfaces/home-page-trip.interface';
import {TripsService} from '../services/trips.service';
import {swiperConfig} from '../config/swiper';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public config: SwiperConfigInterface = swiperConfig;
  homeTrips: HomePageTrip[];

  constructor(
    private loginService: AuthService,
    private route: Router,
    private tripsService: TripsService,
  ) { }

  ngOnInit() {
    if (this.loginService.isAuth()) {
      this.route.navigate(['trips']);
    }
    this.tripsService.getHomeTrips()
      .subscribe((data): HomePageTrip[] => {
        return this.homeTrips = data;
      });
  }

}
